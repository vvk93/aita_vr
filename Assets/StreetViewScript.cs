using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class StreetViewScript : MonoBehaviour
{
    // before 40.7599398,-73.9841319
    // after 40.7601039,-73.9840179
    public static float Longitude = 51.5008906f;
    public static float Latitude = -0.1242708f;
    public static float longStep = 51.5010059f;
    public static float latStep = -0.1239921f;
    public static string Message1 = "";
    public static string Message2 = "";
    public GameObject CameraLeft;
    public GameObject CameraRight;
    public DynamicText Text1;
    public DynamicText Text2;
    public DynamicText Test;
    public Image Image;
    //public GameObject Arrow;

    private Dictionary<string, Texture2D> textures = new Dictionary<string, Texture2D>();
    private List<StreetViewSide> sides = new List<StreetViewSide>();
    private bool canWalk = true;
    private bool canReturn = false;

    void Start()
    {
        Text1.SetText(Message1);
        Text2.SetText(Message2);

        sides.Add(new StreetViewSide(0, 0, "_FrontTex"));
        sides.Add(new StreetViewSide(90, 0, "_LeftTex"));
        sides.Add(new StreetViewSide(180, 0, "_BackTex"));
        sides.Add(new StreetViewSide(270, 0, "_RightTex"));
        sides.Add(new StreetViewSide(0, 90, "_UpTex"));
        sides.Add(new StreetViewSide(0, -90, "_DownTex"));

        StartCoroutine(LoadStreetView());
    }

    public void Return()
    {
        Debug.Log("Clicked return");
        Application.LoadLevel("MainScene");
    }

    private IEnumerator LoadStreetView()
    {
        CameraLeft.GetComponent<BlurEffect>().enabled = true;
        CameraRight.GetComponent<BlurEffect>().enabled = true;
        textures.Clear();

        foreach (var s in sides)
        {
            yield return StartCoroutine(GetStreetviewTexturePitch(s));
        }

        RenderSettings.skybox = CreateSkybox();

        CameraLeft.GetComponent<BlurEffect>().enabled = false;
        CameraRight.GetComponent<BlurEffect>().enabled = false;

        Image.gameObject.SetActive(false);
        Text1.gameObject.SetActive(true);
    }

    private IEnumerator GetStreetviewTexturePitch(StreetViewSide side)
    {
        string url = string.Format("https://maps.googleapis.com/maps/api/streetview?size=1024x1024&location={0},{1}&fov=90&heading={2}&pitch={3}",
            Longitude, Latitude, side.Heading, side.Pitch);

        //url += "&key=AIzaSyDK5UWgPXqD5cOFpgf4HSrkydgfcVx9-QU";

        WWW www = new WWW(url);
        yield return www;

        if (!string.IsNullOrEmpty(www.error))
            Debug.Log("Panorama " + name + ": " + www.error);
        else
            print("Panorama " + name + " loaded url " + url);

        textures.Add(side.Name, www.texture);
    }

    // Update is called once per frame
    void Update()
    {
        Test.SetText(Cardboard.SDK.HeadPose.Position.y.ToString());
        
        if (Cardboard.SDK.HeadPose.Position.y < -0.07f)
            canReturn = true;
        else canReturn = false;


        if (Cardboard.SDK.Triggered && canReturn)
        {
            Application.LoadLevel("MainScene");
        }
        else if (Cardboard.SDK.Triggered && canWalk)
        {
            Longitude = longStep;
            Latitude = latStep;
            StartCoroutine(LoadStreetView());
            canWalk = false;
            //Arrow.gameObject.SetActive(false);
        }
    }

    private IEnumerator GetStreetviewTexture(int degree)
    {
        string url = string.Format("https://maps.googleapis.com/maps/api/streetview?size=1024x1024&location=40.720032,-73.988354&fov=90&heading={0}&pitch=0", degree);

        WWW www = new WWW(url);
        yield return www;

        if (!string.IsNullOrEmpty(www.error))
            Debug.Log("Panorama " + name + ": " + www.error);
        else
            print("Panorama " + name + " loaded url " + url);

        textures.Add(GetSide(degree), www.texture);
    }

    private Material CreateSkybox()
    {
        Material result = new Material(Shader.Find("RenderFX/Skybox"));

        foreach (var k in textures.Keys)
        {
            result.SetTexture(k, textures[k]);
        }

        return result;
    }

    private string GetSide(int degree)
    {
        switch (degree)
        {
            case 0:
                return "_FrontTex";
            case 90:
                return "_LeftTex";
            case 180:
                return "_BackTex";
            case 270:
                return "_RightTex";
            default:
                return "_FrontTex";
        }
    }

    private IEnumerator GetStreetviewTexture()
    {
        string url = "https://maps.googleapis.com/maps/api/streetview?size=1024x1024&location=40.720032,-73.988354&fov=90&heading=235&pitch=10";

        WWW www = new WWW(url);
        yield return www;
        if (!string.IsNullOrEmpty(www.error))
            Debug.Log("Panorama " + name + ": " + www.error);
        else
            print("Panorama " + name + " loaded url " + url);
        //skyMaterial.mainTexture = www.texture;
        RenderSettings.skybox = CreateSkybox(www.texture);


        //RenderSettings.skybox.SetTexture("_FrontTex", www.texture);
        //RenderSettings.skybox.SetTexture("_BackTex", www.texture);
        //RenderSettings.skybox.SetTexture("_LeftTex", www.texture);
        //RenderSettings.skybox.SetTexture("_RightTex", www.texture);
        //RenderSettings.skybox.SetTexture("_UpTex", www.texture);
        //RenderSettings.skybox.SetTexture("_DownTex", www.texture);
        //textures[0] = www.texture;
        //textures[1] = www.texture;
        //textures[2] = www.texture;
        //textures[3] = www.texture;
        //textures[4] = www.texture;
        //textures[5] = www.texture;
        //GetComponent<Renderer>().material.mainTexture = www.texture;
    }

    private Material CreateSkybox(Texture2D texture2D)
    {
        Material result = new Material(Shader.Find("RenderFX/Skybox"));
        result.SetTexture("_FrontTex", texture2D);
        result.SetTexture("_BackTex", texture2D);
        result.SetTexture("_LeftTex", texture2D);
        result.SetTexture("_RightTex", texture2D);
        result.SetTexture("_UpTex", texture2D);
        result.SetTexture("_DownTex", texture2D);
        return result;
    }

    public static Material CreateSkyboxMaterial(SkyboxManifest manifest)
    {
        Material result = new Material(Shader.Find("RenderFX/Skybox"));
        result.SetTexture("_FrontTex", manifest.textures[0]);
        result.SetTexture("_BackTex", manifest.textures[1]);
        result.SetTexture("_LeftTex", manifest.textures[2]);
        result.SetTexture("_RightTex", manifest.textures[3]);
        result.SetTexture("_UpTex", manifest.textures[4]);
        result.SetTexture("_DownTex", manifest.textures[5]);
        return result;
    }

    void SetSkybox(Material material)
    {
        GameObject camera = Camera.main.gameObject;
        Skybox skybox = camera.GetComponent<Skybox>();
        if (skybox == null)
            skybox = camera.AddComponent<Skybox>();
        skybox.material = material;
    }
}

public struct SkyboxManifest
{
    public Texture2D[] textures;

    public SkyboxManifest(Texture2D front, Texture2D back, Texture2D left, Texture2D right, Texture2D up, Texture2D down)
    {
        textures = new Texture2D[6]
         {
             front,
             back,
             left,
             right,
             up,
             down
         };
    }
}

public class StreetViewSide
{
    public int Heading { get; set; }

    public int Pitch { get; set; }

    public string Name { get; set; }

    public StreetViewSide(int heading, int pitch, string name)
    {
        Heading = heading;
        Pitch = pitch;
        Name = name;
    }
}
