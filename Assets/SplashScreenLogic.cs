﻿using UnityEngine;
using System.Collections;

public class SplashScreenLogic : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        StartCoroutine(LoadMainScene());
    }
    public IEnumerator LoadMainScene()
    {
        yield return new WaitForSeconds(2.5f);
        Application.LoadLevel("MainScene");
    }
    // Update is called once per frame
    void Update()
    {

    }
}
