﻿using UnityEngine;
using System.Collections;

public class MainLogicScript : MonoBehaviour
{
    public GameObject Camera;

    public GameObject Destination1;
    public GameObject Destination2;
    public GameObject Destination3;
    public GameObject Destination4;

    private static int step = 0;
    // Use this for initialization
    void Start()
    {
        SetPosition();
    }

    private void SetPosition()
    {
        switch (step)
        {
            case 1:
                Camera.transform.position = new Vector3(Destination1.transform.position.x, 
                    Camera.transform.position.y, Destination1.transform.position.z);
                break;
            case 2:
                Camera.transform.position = new Vector3(Destination2.transform.position.x, 
                    Camera.transform.position.y, Destination2.transform.position.z);
                break;
            case 3:
                Camera.transform.position = new Vector3(Destination3.transform.position.x, 
                    Camera.transform.position.y, Destination3.transform.position.z);
                break;
            case 4:
                Camera.transform.position = new Vector3(Destination4.transform.position.x,
                    Camera.transform.position.y, Destination4.transform.position.z);
                break;
            default:
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Cardboard.SDK.Triggered && !LandmarkLogic.IS_GAZED)
        {
            MoveToNextPoint();
        }
    }

    private void MoveToNextPoint()
    {
        switch (step)
        {
            case 0:
                iTween.MoveTo(Camera, new Vector3(Destination1.transform.position.x,
                    Camera.transform.position.y, Destination1.transform.position.z), 2.5f);
                step++;
                break;
            case 1:
                iTween.MoveTo(Camera,
                    new Vector3(Destination2.transform.position.x,
                    Camera.transform.position.y, Destination2.transform.position.z), 5.5f);
                step++;
                break;
            case 2:
                iTween.MoveTo(Camera, new Vector3(Destination3.transform.position.x,
                    Camera.transform.position.y, Destination3.transform.position.z), 5.5f);
                step++;
                break;
            case 3:
                iTween.MoveTo(Camera, new Vector3(Destination4.transform.position.x,
                    Camera.transform.position.y, Destination4.transform.position.z), 5.5f);
                step++;
                break;
            default:
                break;
        }
    }
}
