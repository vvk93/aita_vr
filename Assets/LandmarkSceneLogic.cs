﻿using UnityEngine;
using System.Collections;

public class LandmarkSceneLogic : MonoBehaviour
{
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        //if (Cardboard.SDK.Triggered)
        //{
        //    Application.LoadLevel("MainScene");
        //}
    }

    public void ReturnToMainScene()
    {
        Application.LoadLevel("MainScene");
    }

    private IEnumerator GetStreetviewTexture()
    {
        string url = "https://maps.googleapis.com/maps/api/streetview?size=600x300&location=46.414382,10.013988&heading=151.78&pitch=-0.76";

        //if (key != "")
        //    url += "&key=" + key;

        WWW www = new WWW(url);
        yield return www;
        if (!string.IsNullOrEmpty(www.error))
            Debug.Log("Panorama " + name + ": " + www.error);
        else
            print("Panorama " + name + " loaded url " + url);

        GetComponent<Renderer>().material.mainTexture = www.texture;
    }
}
