﻿using UnityEngine;
using System.Collections;

public class LandmarkTextLogic : MonoBehaviour
{
    public GameObject Text1;
    public GameObject Text2;
    // Use this for initialization
    void Start()
    {
        StartCoroutine(ChangeText());
    }

    public IEnumerator ChangeText()
    {
        yield return new WaitForSeconds(5.0f);
        iTween.FadeTo(Text1, 0, 1.0f);
        Text1.gameObject.SetActive(false);
        Text2.gameObject.SetActive(true);
        iTween.FadeTo(Text2, 0, 8.0f);
    }
    // Update is called once per frame
    void Update()
    {

    }
}
