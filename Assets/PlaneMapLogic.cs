﻿using UnityEngine;
using System.Collections;

public class PlaneMapLogic : MonoBehaviour
{

    // Use this for initialization
    IEnumerable Start()
    {
        var url = "http://maps.google.com/maps/api/staticmap?center=42.512444,90.168347&zoom=4&size=1800x1600&type=hybrid&sensor=true";
        Debug.Log(url);
        WWW www = new WWW(url);
        yield return www;
        GetComponent<Renderer>().material.mainTexture = www.texture;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.DrawLine(Vector3.zero, new Vector3(1, 0, 0), Color.red);
    }
}
