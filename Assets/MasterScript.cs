﻿using UnityEngine;
using System.Collections;

public class MasterScript : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private bool created;
    void Awake()
    {
        if (!created)
        {
            DontDestroyOnLoad(transform.gameObject);
            created = true;
        }

        else
        {
            Destroy(this.gameObject);
        }
    }

    void OnLevelWasLoaded(int level)
    {
        if (level == 0 && created)
        {
            Destroy(transform.gameObject);
            created = false;
        }
    }
}
