﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class LandmarkLogic : MonoBehaviour
{

    public Vector3 initialPos;
    public static bool IS_GAZED = false;
    public GameObject Text;
    public string Name;
    public string Place;

    // Use this for initialization
    void Start()
    {
        SetGazedAt(false);
        //Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetGazedAt(bool gazedAt)
    {
        //Vector3 pos = gazedAt ? new Vector3(initialPos.x, 6, initialPos.z) : initialPos;

        //iTween.PunchScale(gameObject, new Vector3(0.4f, 0.4f, 0.4f), 1.5f);
        if (gazedAt)
        {
            Text.GetComponent<DynamicText>().SetText(Name + System.Environment.NewLine + Place);
            Text.gameObject.SetActive(true);
            //iTween.ScaleAdd(gameObject, new Vector3(2, 1f, 0), 2f);
            IS_GAZED = true;
        }
        else
        {
            Text.gameObject.SetActive(false);
            //iTween.ScaleTo(gameObject, new Vector3(6, 4, 0.1f), 2f);
            IS_GAZED = false;
        }
    }

    public void LoadLandmarkScene()
    {
        var name = this.name;

        switch (name)
        {
            case "LondonLandmark":
                StreetViewScript.Longitude = 51.5011388f;
                StreetViewScript.Latitude = -0.1239707f;
                StreetViewScript.longStep = 51.5010059f;
                StreetViewScript.latStep = -0.1239921f;
                StreetViewScript.Message1 = @"Big Ben is the nickname for the Great Bell 
of the clock at the north end of the
Palace of Westminster in London";
                StreetViewScript.Message2 = @"The tower was completed in 1858 and 
had its 150th anniversary on 31 May 2009";
                break;
            case "CorkLandmark":
                StreetViewScript.Longitude = 51.9000308f;
                StreetViewScript.Latitude = -8.4027308f;
                StreetViewScript.longStep = 51.8993445f;
                StreetViewScript.latStep = -8.4035243f;
                StreetViewScript.Message1 = @"Blackrock Castle is a 16th-century castle
 located about 2 km from the heart of Cork city, 
Ireland on the banks of the River Lee";
                StreetViewScript.Message2 = @"Originally built to defend the port and 
upper flows of Cork Harbour, the castle 
is now the site of an observatory";
                break;
            case "NYLandmark":
                StreetViewScript.Longitude = 40.7098722f;
                StreetViewScript.Latitude = -73.990047f;
                StreetViewScript.longStep = 40.7087112f;
                StreetViewScript.latStep = -73.9915062f;
                StreetViewScript.Message1 = @"The Manhattan Bridge is a suspension bridge
 that crosses the East River in New York City";
                StreetViewScript.Message2 = @"The main span is 1,470 ft (448 m) long, with
the suspension cables being 3,224 ft (983 m) long";
                break;
            case "RushmoreLandmark":
                StreetViewScript.Longitude = 43.8772048f;
                StreetViewScript.Latitude = -103.4559586f;
                StreetViewScript.longStep = 43.8780174f;
                StreetViewScript.latStep = -103.4581122f;
                StreetViewScript.Message1 = @"Rushmore National Memorial is a sculpture
carved into the granite face of Mount Rushmore
 near Keystone, South Dakota";
                StreetViewScript.Message2 = @"Mount Rushmore features 18 m 
sculptures of the heads of four US presidents: 
Washington, Jefferson, Roosevelt and Lincoln";
                break;
            case "CapitolLandmark":
                StreetViewScript.Longitude = 38.5770888f;
                StreetViewScript.Latitude = -121.4952513f;
                StreetViewScript.longStep = 38.5770181f;
                StreetViewScript.latStep = -121.4948888f;
                StreetViewScript.Message1 = @"The California State Capitol 
is home to the government of California";
                StreetViewScript.Message2 = @"Located in Sacramento, the Neoclassical 
structure was completed between 1861 
and 1874 at the west end of Capitol Park";
                break;
            default:
                StreetViewScript.Longitude = 51.5011388f;
                StreetViewScript.Latitude = -0.1239707f;
                StreetViewScript.longStep = 51.5010059f;
                StreetViewScript.latStep = -0.1239921f;
                StreetViewScript.Message1 = "";
                StreetViewScript.Message2 = "";
                break;
        }

        Application.LoadLevel("TestSV");
    }
}
