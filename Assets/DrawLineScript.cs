﻿using UnityEngine;
using System.Collections;

public class DrawLineScript : MonoBehaviour
{

    public GameObject StartPoint;
    public GameObject FinishPoint;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3[] path = new Vector3[2];
        path[0] = StartPoint.transform.position;
        path[1] = FinishPoint.transform.position;
        //iTween.DrawLineGizmos(path, Color.red);
        iTween.DrawPathHandles(path);
    }

    void OnDrawGizmos()
    {
        //Vector3[] path = new Vector3[2];
        //path[0] = StartPoint.transform.position;
        //path[1] = FinishPoint.transform.position;
        //iTween.DrawPath(path, Color.red);
    }
}
