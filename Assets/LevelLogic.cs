﻿using UnityEngine;
using System.Collections;

public class LevelLogic : MonoBehaviour
{
    private LineRenderer lineRenderer;
    private float distance;
    private float counter;

    public GameObject StartPoint;
    public GameObject FinishPoint;
    public int DrawSpeed;

    // Use this for initialization
    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        //lineRenderer.material = new Material(Shader.Find("Particles/Additive"));
        lineRenderer.SetWidth(1.5f, 1.5f);
        lineRenderer.SetPosition(0, StartPoint.transform.position);

        distance = Vector3.Distance(StartPoint.transform.position,
            FinishPoint.transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        if (counter < distance)
        {
            counter += 0.1f / DrawSpeed;

            var x = Mathf.Lerp(0, distance, counter);

            var point = x * Vector3.Normalize(FinishPoint.transform.position - StartPoint.transform.position)
                + StartPoint.transform.position;

            lineRenderer.SetPosition(1, point);
        }
    }
}
