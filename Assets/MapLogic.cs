﻿using UnityEngine;
using System.Collections;

public class MapLogic : MonoBehaviour {

	// Use this for initialization
    IEnumerator Start()
    {
        LocationInfo li = new LocationInfo();
        var url = "http://maps.google.com/maps/api/staticmap?center=42.512444,90.168347&zoom=4&size=800x600&type=hybrid&sensor=true";
        Debug.Log(url);
        WWW www = new WWW(url);
        yield return www;
        GetComponent<Renderer>().material.mainTexture = www.texture;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
